﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTarget : MonoBehaviour {

	public Transform targetToFollow;


	void Update () {
		this.transform.position = new Vector3 (targetToFollow.position.x, targetToFollow.position.y, this.transform.position.z);
		
	}
}
