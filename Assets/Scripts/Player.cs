﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour {

	public float maxS = 11f;
	private Rigidbody2D rb2d = null;
	private float move = 0f;
	private Animator anim;
	private bool flipped = false;

	private bool muerto = false;
	public AudioSource audio;
	public AudioSource audioDead;
	public AudioSource audiosword;

	private bool isCoroutine= false;

	private bool attack;

	[SerializeField]
	private Transform[] groundpoints;

	[SerializeField]
	private float groundRadius;

	[SerializeField]
	private LayerMask whatIsGround;

	private bool isGrounded;

	private bool jump;

	[SerializeField]
	private bool airControl;

	[SerializeField]
	private float jumpForce;

	void Awake(){
		rb2d = GetComponent<Rigidbody2D> ();
		anim = GetComponent<Animator> ();
	}

	void Update(){
		HandleInput ();
	}

	void FixedUpdate()
	{
		move = Input.GetAxis ("Horizontal");

		isGrounded = IsGrounded ();

		Movement ();


		Attack ();

		HandleLayers ();

		ResetValue ();

		if (rb2d.velocity.x > 0.001f || rb2d.velocity.x < -0.001f) {
			if ((rb2d.velocity.x < -0.001f && !flipped) || (rb2d.velocity.x > -0.001f && flipped)){
				flipped = !flipped;
			this.transform.rotation = Quaternion.Euler (0, flipped ? 180 : 0, 0);
		}
		anim.SetBool ("Running", true);
	}else{
		anim.SetBool("Running", false);
		}
	}

	private void Movement(){

		if (rb2d.velocity.y < 0) {
			anim.SetBool ("Land", true);
		}

		
		if (!this.anim.GetCurrentAnimatorStateInfo(0).IsTag("Attacking")&&(isGrounded||airControl)) {
			
			rb2d.velocity = new Vector2 (move * maxS, rb2d.velocity.y);
		}
		if (isGrounded && jump) {
			isGrounded = false;
			rb2d.AddForce (new Vector2 (0, jumpForce));
			anim.SetTrigger ("Jump");
			audio.Play ();
		}
	}

	private void Attack(){
		if (attack && !this.anim.GetCurrentAnimatorStateInfo(0).IsTag("Attacking")) {
			anim.SetTrigger ("Attacking");
			audiosword.Play ();
			rb2d.velocity = Vector2.zero;
		}
	}

	private void HandleInput(){
		if (Input.GetKeyDown(KeyCode.Space)) {
			attack = true;
		}
		if (Input.GetKeyDown (KeyCode.UpArrow)) {
			jump = true;
		}
	}

	private void ResetValue(){
		attack = false;
		jump = false;
	}

	void OnCollisionEnter2D(Collision2D coll){
		if(coll.gameObject.tag == "muerte"){
			rb2d.velocity = Vector2.zero;
			muerto=true;
			anim.SetBool("dying",true);
			audioDead.Play ();
			Invoke("Dead", 3);
		}
	}

	private bool IsGrounded(){
		if (rb2d.velocity.y <= 0) {
			foreach (Transform point in groundpoints) {
				Collider2D[] colliders = Physics2D.OverlapCircleAll (point.position, groundRadius, whatIsGround);

				for (int i = 0; i < colliders.Length; i++) {
					if (colliders [i].gameObject != gameObject) {
						anim.ResetTrigger ("Jump");
						anim.SetBool ("Land", false);
						return true;
					}
				}
			}
				
		}
		return false;
	}

	private void HandleLayers()
	{
		if (!isGrounded) {
			anim.SetLayerWeight (1, 1);
		} else {
			anim.SetLayerWeight (1, 0);
		}
	}

	void Dead (){
		SceneManager.LoadScene ("GameOver");
	}
}